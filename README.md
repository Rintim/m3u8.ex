# M3u8

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `m3u8` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:m3u8, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/m3u8](https://hexdocs.pm/m3u8).

