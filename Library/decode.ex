defmodule M3U8.Decode do

  import Standard

  begin "Function Defination" do

    begin {:parse, 1} do

      def parse(file: file), do:
        with {:ok, body} <- File.read(file), do:
          parse(body, Path.dirname(file))

      def parse(url: links) do
        with {:ok, %HTTPoison.Response{body: body}} <- HTTPoison.get(links, ["User-Agent": "Mozilla/5.0 (X11; FreeBSD amd64; rv:81.0) Gecko/20100101 Firefox/81.0"]) do
          dir =
            links
            |> String.split("/")
            |> then(fn str ->
              len = length(str)
              str
              |> Enum.slice(0..len-2)
            end)
            |> Enum.join("/")
          #parse(body, (with %{"dir" => dir} <- Regex.named_captures(~r/(?<dir>http([s]?):\/\/.*).+.m3u8.*/, links), do: dir))
          #IO.puts(dir)
          parse(body, dir)
        end
      end

      def parse(body) do
        body
        |> String.split("\n")
        |> Enum.filter(fn line ->
          #IO.puts(line)
          pipe([
            line,
            {2, Regex.match?(~r/^\#/)},
          ]) == false && pipe([
            line,
            {2, Regex.match?(~r/^\\n$/)},
          ]) == false
        end)
        #|> Enum.map(fn line ->
        #  Regex.replace(~r/\/#EXT.+?$/, line, "")
        #end)
      end

    end

    begin "Parse (2)" do

      def parse(body, dir) do
        parse(body)
        |> Enum.map(fn line ->
          if Regex.match?(~r/^http/, line), do: line,
          else: if Regex.match?(~r/.*\/$/, dir), do: dir <> line, else: dir <> "/" <> line
        end)
      end

    end

  end

end
